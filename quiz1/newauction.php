<?php 
require_once 'db.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>New Auction</title>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector:'textarea'});</script>
    <link rel="stylesheet" href="styles.css" />
</head>
<body>
    <h1 class="centered">New Auction</h1>
    <div id="centerArticle">
    <?php

        function printForm($name="", $email="", $body = "", $price="") {
            $form = <<< END
            <form method="post" enctype="multipart/form-data">
                <table>
                    <tr>
                        <td>
                            Seller's Name: 
                        </td>
                        <td>
                            <input type="text" name="name" value="$name">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Seller's E-Mail: 
                        </td>
                        <td>
                            <input type="email" name="email" value="$email">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Item Photo (<i>optional</i>): 
                        </td>
                        <td>
                        <input type="file" name="photo">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea name="body" cols="60" rows="10">$body</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Initial Bid Price: 
                        </td>
                        <td>
                            <input type="number" min="0" step="0.01" name="price" value="$price">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" name="submit" value="Sumbit">
                        </td>
                    </tr>
                </table>
            </form>
            END;
            echo $form;
        }

        if (isset($_POST['submit'])) { // are we receiving a submission?
            $name = $_POST['name'];
            $email = $_POST['email'];
            $body = $_POST['body'];
            $price = $_POST['price'];
            $body = strip_tags($body, "<p><ul><ol><li><br><hr><em><i><strong><bold><span>");
            $errorList = array();
            if (strlen($name) < 2 || strlen($name) > 100){ // || preg_match('/^[a-zA-Z0-9 \-\.\,]{2-100}$/', $name) != 1
                array_push($errorList, "Name must be 2-100 characters, only letters (upper/lower-case), space, dash, dot, comma and numbers allowed");
                $name = "";
            }
            if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
                array_push($errorList, "Email does not appear to be valid");
                $email = "";
            }
            $photoFilePath = null;  // in SQL INSERT query this must become NULL and *not* 'NULL'
            if (isset($_FILES['photo']) && $_FILES['photo']['error'] != 4) { // file uploaded
            // print_r($_FILES);
            $photo = $_FILES['photo'];
            if ($photo['error'] != 0) {
                $errorList[] = "Error uploading photo " . $photo['error'];
            } else {
                if ($photo['size'] > 1024*1024) { // 1MB
                    $errorList[] = "File too big. 1MB max is allowed.";
                } else {
                    $info = getimagesize($photo['tmp_name']);
                    if (!$info) {
                        $errorList[] = "File is not an image";
                    } else {
                        if ($info[0] < 200 || $info[0] > 1000 || $info[1] < 200 || $info[1] > 1000) {
                            $errorList[] = "Width and height must be within 200-1000 pixels range";
                        } else {
                            $ext = "";
                            switch ($info['mime']) {
                                case 'image/jpeg': $ext = "jpg"; break;
                                case 'image/gif': $ext = "gif"; break;
                                case 'image/bmp': $ext = "bmp"; break;
                                case 'image/png': $ext = "png"; break;
                                default:
                                    $errorList[] = "Only JPG, GIF and PNG file types are allowed";
                            }
                            if ($ext) {
                                $fileName = pathinfo($_FILES['photo']['name'], PATHINFO_FILENAME);
                                $photoFilePath = "uploads/" . $fileName . "." . $ext;   //  ERROR HERE
                            }
                        }
                    }
                }
            }
        }
            if (strlen($body) < 2 || strlen($body) > 1000) {
                array_push($errorList, "Body must be 2-1000 characters long");
            }
            if ($price < 0) {
                array_push($errorList, "Price must be 0 or greater");
                $price = "";
            }
            if ($errorList) { // STATE 2: errors in submission - failed
                echo "<p>There were problems with your submission:</p>\n<ul>\n";
                foreach ($errorList as $error) {
                    echo "<li class=\"errorMessage\">$error</li>\n";
                }
                echo "</ul>\n";
                printForm($name, $email, $body);
            } else { // STATE 3: successful submission
                // On successful submission you need to insert a new record
                $result = mysqli_query($link, sprintf("INSERT INTO auctions VALUES (NULL, '%s', %s, '%s', '%s', '%s', null, null)",
                    mysqli_real_escape_string($link, $body),
                    ($photoFilePath == null) ? "NULL" : "'" . mysqli_real_escape_string($link, $photoFilePath) . "'",
                    mysqli_real_escape_string($link, $name),
                    mysqli_real_escape_string($link, $email),
                    mysqli_real_escape_string($link, $price),
                ));
                if (!$result) {
                    echo "SQL Query failed: " . mysqli_error($link);
                    exit;
                }
                if ($photoFilePath != null) {
                    move_uploaded_file($_FILES['photo']['tmp_name'], $photoFilePath);
                }
                $auctionId = mysqli_insert_id($link);
                echo "<p>Auction successfully added</p>";
                echo '<p><a href="placebid.php?id='. $auctionId .'">Click here to view the new auction</a></p>';
            }
        } else { // STATE 1: first display
            printForm();
        }
    ?>
    </div>
</body>
</html>