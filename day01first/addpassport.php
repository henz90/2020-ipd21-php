<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div id="centeredContent">
<?php
    require_once 'db.php';
    function printForm($passportNo = "") {
        $passportNo = htmlentities($passportNo);
        $form = <<< END
        <form action="upload.php" method="post" enctype="multipart/form-data">
        <table>
            <tr>
                <td>
                    Passport Number: 
                </td>
                <td>
                    <input type="text" name="passportNo" value="$passportNo">
                </td>
            </tr>
            <tr>
                <td>
                    Passport Photo:
                    </br>
                    <i>(optional)</i>
                </td>
                <td>
                <input type="file" name="file">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="submit" value="Upload Passport Information">
                </td>
            </tr>
        </table>
            
        </form>
        END;
        echo $form;
    }
    if (isset($_POST['passportNo'])) { // are we receiving a submission?
        $passportNo = $_POST['passportNo'];
        $errorList = array();
        if (preg_match('/^[A-Z]{2}[0-9]{6}$/', $passportNo) != 1) {
            array_push($errorList, "Passport number must be in AB123456 format");
        }
        // TODO: verify the picture upload is acceptable
        $photoFilePath = null;  // in SQL INSERT query this must become NULL and *not* 'NULL'
        if (isset($_FILES['photo']) && $_FILES['photo']['error'] != 4) { // file uploaded
            // print_r($_FILES);
            $photo = $_FILES['photo'];
            if ($photo['error'] != 0) {
                $errorList[] = "Error uploading photo " . $photo['error'];
            } else {
                if ($photo['size'] > 1024*1024) { // 1MB
                    $errorList[] = "File too big. 1MB max is allowed.";
                } else {
                    $info = getimagesize($photo['tmp_name']);
                    if (!$info) {
                        $errorList[] = "File is not an image";
                    } else {
                        if ($info[0] < 200 || $info[0] > 1000 || $info[1] < 200 || $info[1] > 1000) {
                            $errorList[] = "Width and height must be within 200-1000 pixels range";
                        } else {
                            $ext = "";
                            switch ($info['mime']) {
                                case 'image/jpeg': $ext = "jpg"; break;
                                case 'image/gif': $ext = "gif"; break;
                                case 'image/png': $ext = "png"; break;
                                default:
                                    $errorList[] = "Only JPG, GIF and PNG file types are allowed";
                            }
                            if ($ext) {
                                $photoFilePath = "uploads/" .  $passportNo . "." . $ext;
                            }
                        }
                    }
                }
            }
        }
        // it's okay if no photo was selected - we will just insert NULL value
        //
        if ($errorList) { // STATE 2: errors in submission - failed
            echo "<p>There were problems with your submission:</p>\n<ul>\n";
            foreach ($errorList as $error) {
                echo "<li class=\"errorMessage\">$error</li>\n";
            }
            echo "</ul>\n";
            printForm($passportNo);
        } else { // STATE 3: successful submission
            $sql = sprintf("INSERT INTO passports VALUES (NULL, '%s', %s)",
                mysqli_real_escape_string($link, $passportNo),
                ($photoFilePath == null) ? "NULL" : "'" . mysqli_real_escape_string($link, $photoFilePath) . "'");
            $result = mysqli_query($link, $sql);
            if (!$result) {
                echo "SQL Query failed: " . mysqli_error($link);
                exit;
            }
            if ($photoFilePath != null) {
                move_uploaded_file($_FILES['photo']['tmp_name'], $photoFilePath);
            }
            echo "<p>Passport successfully added</p>";
        }
    } else { // STATE 1: first display
        printForm();
    }
?>
</body>
</html>