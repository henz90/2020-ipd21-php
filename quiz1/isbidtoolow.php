<?php
require_once 'db.php';

if (!isset($_GET['price'])){
    die("Error: Price parameter Missing"); //  PERL
}

$price = mysqli_real_escape_string($link,$_GET['price']);
$sql = "SELECT * from users WHERE lastBidPrice='$price'";
$result = mysqli_query($link, $sql);

if (!$result){
    die("SQL Query Failed: ". mysqli_error($link));
}

$lastBidPrice = mysqli_fetch_assoc($result);

if ($price < $lastBidPrice) {
    echo "Bid is too low";
} else {
    echo "";    //No Output if price is fine
}
?>