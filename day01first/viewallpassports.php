<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css" />
</head>
<body>
<div id="centeredContent">
    <table border=1>
        <tr><th>#</th><th>Passport No.</th><th>Photo</th></tr>
    <?php
        require_once 'db.php';
        $sql = "SELECT * FROM passports";
        $result = mysqli_query($link, $sql);
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        while ($row = mysqli_fetch_assoc($result)) {
            // print_r($row);
            printf("<tr><td>%d</td><td>%s</td><td>%s</td></tr>\n",
            $row['id'], $row['passportNo'],
            $row['photoFilePath'] ? "<img width=150 src=" . $row['photoFilePath'] . ">" : "");
        }
    ?>
    </table>
</div>
</body>
</html>