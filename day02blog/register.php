<?php 
require_once 'db.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 
    <script>
        $(document).ready(function() {
            //  alert("jQuery Works");
            $('input[name=email]').on('paste blur change input',function() {
                var email = $('input[name=email]').val();
                $("#emailTaken").load("isemailtaken.php?email="+email);
            });

            $(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log("Ajax error occured on " + settings.url);
                alert("Ajax error occured");
            });

        });
    </script>
</head>
<body>
<h1 class="centered">Register</h1>
    <div id="center">
    <?php
        function printForm($user = "", $email = "") {
            $user = htmlentities($user); // avoid invalid html in case <>" are part of name
            $email = htmlentities($email); // just be safe but perhaps not needed for an email
            $form = <<< END
            <form method="post">
                <table>
                    <tr>
                        <td>
                            Username: 
                        </td>
                        <td>
                            <input type="text" name="user" value="$user">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            E-Mail: 
                        </td>
                        <td>
                            <input type="text" name="email" value="$email">
                            <span id="emailTaken" class="errorMessage"></span></br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password: 
                        </td>
                        <td>
                        <input type="password" name="pass1">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password: 
                        </td>
                        <td>
                        <input type="password" name="pass2">
                        </td>
                    </tr>
                </table>
                <input type="submit" value="Register">
            </form>
END;
            echo $form;
        }

        if (isset($_POST['user'])) { // are we receiving a submission?
            $user = $_POST['user'];
            $email = $_POST['email'];
            $pass1 = $_POST['pass1'];
            $pass2 = $_POST['pass2'];
            //
            $errorList = array();
            if (preg_match('/^[a-zA-Z0-9 \\._\'"-]{4,50}$/', $user) != 1) { // no match
                array_push($errorList, "Name must be 4-50 characters long and consist of letters, digits, "
                    ."spaces, dots, underscores, apostrophies, or minus sign.");
                $user = "";
            }
            if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
                array_push($errorList, "Email does not look valid");
                $email = "";
            } else {
                // is email already in use?
                $result = mysqli_query($link, sprintf("SELECT * FROM users WHERE email='%s'",
                    mysqli_real_escape_string($link, $email)));
                if (!$result) {
                    echo "SQL Query failed: " . mysqli_error($link);
                    exit;
                }
                $userRecord = mysqli_fetch_assoc($result);
                if ($userRecord) {
                    array_push($errorList, "This email is already registered");
                    $email = "";
                }
            }
            if ($pass1 != $pass2) {
                array_push($errorList, "Passwords do not match");
            } else {
                if ((strlen($pass1) < 6) || (strlen($pass1) > 100)
                        || (preg_match("/[A-Z]/", $pass1) == FALSE )
                        || (preg_match("/[a-z]/", $pass1) == FALSE )
                        || (preg_match("/[0-9]/", $pass1) == FALSE )) {
                    array_push($errorList, "Password must be 6-100 characters long, "
                        . "with at least one uppercase, one lowercase, and one digit in it");
                }
            }
            //
            if ($errorList) { // STATE 2: errors in submission - failed
                echo "<p>There were problems with your submission:</p>\n<ul>\n";
                foreach ($errorList as $error) {
                    echo "<li class=\"errorMessage\">$error</li>\n";
                }
                echo "</ul>\n";
                printForm($user, $email);
            } else { // STATE 3: successful submission                
                $result = mysqli_query($link, sprintf("INSERT INTO users VALUES (NULL, '%s', '%s', '%s')",
                    mysqli_real_escape_string($link, $user),
                    mysqli_real_escape_string($link, $email),
                    mysqli_real_escape_string($link, $pass1)));
                if (!$result) {
                    echo "SQL Query failed: " . mysqli_error($link);
                    exit;
                }
                echo "<p>Registration successful</p>";
                echo '<p><a href="login.php">Click here to login</a></p>';
            }
        } else { // STATE 1: first display
            printForm();
        }

    ?>
    </div>
</body>
</html>