<?php 
require_once 'db.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Place a Bid</title>
    <link rel="stylesheet" href="styles.css" />
    <script>
        $(document).ready(function() {
            // alert("jQuery works");
            $('input[name=price]').on('paste blur change input', function() {
                var price = $('input[name=price]').val();
                $("#isbidtoolow").load("isbidtoolow.php?price=" + price);
            });

            $(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log("Ajax error occured on " + settings.url);
                alert("Ajax error occured");
            });
        });
    </script>
</head>
<body>
<h1 class="centered">Auction</h1>
    <div id="centerArticle">
        <table class="centered">
            <tr>
                <th>Seller's Name</th>
                <th>Item Description</th>
                <th>Last Bid Price</th>
                <th>Photo</th>
            </tr>
        <?php
            $lastBidPrice;
            if (!isset($_GET['id'])) {    
                echo "Error: article id missing in the URL";
                exit;
            }
            $id = $_GET['id'];
            $sql = "Select * from auctions where id=".$id;
            $result = mysqli_query($link, $sql);
            if (!$result){
                echo "SQL Query Failed: " . mysqli_error($link);
                exit;
            }
            while ($row = mysqli_fetch_assoc($result)){
                printf("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n",
                    $row['sellersName'],    
                    $row['itemDescription'],
                    "$".$row['lastBidPrice'],
                    $row['itemImagePath'] ? "<img width=150 src=" . $row['itemImagePath'] . ">" : ""
                );
                $lastBidPrice = $row['lastBidPrice'];
            }

            function printForm($name="", $email="", $price="") {
                $form = <<< END
                <form method="post">
                    <table>
                        <tr>
                            <td>
                                Bidder's Name: 
                            </td>
                            <td>
                                <input type="text" name="name" value="$name">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Bidder's E-Mail: 
                            </td>
                            <td>
                                <input type="email" name="email" value="$email">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Bid: 
                            </td>
                            <td>
                                <input type="number" min="0" step="0.01" name="price" value="$price">
                                <span class="errorMessage" id="isbidtoolow"></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" name="submit" value="Sumbit">
                            </td>
                        </tr>
                    </table>
                </form>
                END;
                echo $form;
            }

                if (isset($_POST['submit'])) { // are we receiving a submission?
                    $name = $_POST['name'];
                    $email = $_POST['email'];
                    $price = $_POST['price'];
                    $id = $_GET['id'];
                    $errorList = array();
                    if (strlen($name) < 2 || strlen($name) > 100){ // || preg_match('/^[a-zA-Z0-9 \-\.\,]{2-100}$/', $name) != 1
                        array_push($errorList, "Name must be 2-100 characters, only letters (upper/lower-case), space, dash, dot, comma and numbers allowed");
                        $name = "";
                    }
                    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
                        array_push($errorList, "Email does not appear to be valid");
                        $email = "";
                    }
                  
                    if ($price < 0 || $price < $lastBidPrice) {
                        array_push($errorList, "Price must be greater than $". $lastBidPrice);
                        $price = "";
                    }
                    if ($errorList) { // STATE 2: errors in submission - failed
                        echo "<p>There were problems with your submission:</p>\n<ul>\n";
                        foreach ($errorList as $error) {
                            echo "<li class=\"errorMessage\">$error</li>\n";
                        }
                        echo "</ul>\n";
                        printForm($name, $email, $price);
                    } else { // STATE 3: successful submission
                        // On successful submission you need to insert a new record
                        $result = mysqli_query($link, sprintf("UPDATE auctions SET lastBidPrice = '%d', lastBidderName = '%s', lastBidderEmail = '%s' where id = $id",
                            mysqli_real_escape_string($link, $price),
                            mysqli_real_escape_string($link, $name),
                            mysqli_real_escape_string($link, $email)
                        ));
                        if (!$result) {
                            echo "SQL Query failed: " . mysqli_error($link);
                            exit;
                        }
                        echo "<p>Bid successfully added</p>";
                    }
                } else { // STATE 1: first display
                    printForm();
                }
        ?>
        </table>
    </div>
</body>
</html>