<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css"/>
</head>
<body>
    </br>
    <div id="centeredContent">
    <?php

    require_once 'db.php';

        function isInt($value) {
            return (is_numeric($value) && ((int)$value == $value));
        }

        function displayForm($name = "", $age = ""){
            $form = <<< ENDMARKER
            <form method="post">
            Name: <input name="name" type="text" value="$name">
            </br>
            Age: <input name="age" type="number" value="$age">
            </br>
            <input type="submit" value="Say Hello">
            </form>
            ENDMARKER;
            echo $form;
        }

        // Are we receiving a submission?
        if (isset($_POST['name'])) {    //  Yes - Submission Data Present
            $name = $_POST['name'];
            $age = $_POST['age'];
            //  Verify Inputs
            $errorList = array();
            if (strlen($name) < 2 || strlen($name) > 50){
                array_push($errorList, "Name must be 2-50 characters long");
                $name = "";
            }
            if (!isInt($age) || $age < 0 || $age > 150) {
                array_push($errorList, "Age must be a number between 0 and 150");
                $age = "";
            }
            //
            if ($errorList){    //  STATE 2: Submission with errors (failed)
                displayForm($name, $age);
                echo '<ul class="errorMessage">';
                foreach($errorList as $error) {
                    echo "<li>$error</li>";
                }
                echo '</ul>';
            } else {    //  STATE 3: Successful Submission
                echo "<p> Hello $name, you are $age years old.";
                $sql = sprintf("INSERT INTO people VALUES (NULL, '%s', '%s')",
                    mysqli_escape_string($link, $name),
                    mysqli_escape_string($link, $age)
                );
                if (!mysqli_query($link,$sql)){
                    echo "Fatal Error: Failed to execute SQL Query: ".mysqli_error($link);
                    exit;
                }
                echo "<p>Submission successful, record added to database</p>";
            }
            //  
        } else {
            //  STATE 1: First display of the empty form.
            displayForm();
        }
    ?>
    </div>
</body>
</html>