<?php
session_start(); // enable Sessions mechanism for the entire web application

require_once 'vendor/autoload.php';

DB::$dbName = 'day01people';
DB::$user = 'day01people';
DB::$password = 'oG8Ys6nZDipngHZb';
DB::$host = 'localhost';
DB::$port = 3333;


// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/cache',
        'debug' => true, // This line should enable debug mode
    ]);
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};

// Define app routes
$app->get('/hello/{name}', function ($request, $response, $args) {
    return $response->write("<p>Hello " . $args['name'] . "</p>");
});

$app->get('/hello/{nameX}/{ageX}', function ($request, $response, $args) {
    // DB::insert('people', ['name' => $args['nameX'], 'age' => $args['ageX'] ]);
    // return $response->write("</p>Hello " . $args['nameX'] . " you are " . $args['ageX']. " y/o</p>");
    return $this->view->render($response, 'hello.html.twig', [
        'nameZZZ' => $args['nameX'], 'ageZZZ' => $args['ageX']
    ]);
});

// STATE 1: first display
$app->get('/addperson', function ($request, $response, $args) {
    return $this->view->render($response, 'addperson.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/addperson', function ($request, $response, $args) {
    $name = $request->getParam('name');
    $age = $request->getParam('age');
    //
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) > 50) {
        $errorList[] = "Name must be 2-50 characters long";
    }
    if ($age != (int)$age || empty($age)) {
        $errorList[] = "Age must be an integer number";
    } else if ($age < 0 || $age > 150) {
        $errorList[] = "Age must be within 0-150 range";
    }
    if ($errorList) {
        return $this->view->render($response, 'addperson.html.twig',
                [ 'errorList' => $errorList ]);
    } else {
        DB::insert('people', ['name' => $name, 'age' => $age]);
        return $this->view->render($response, 'addperson_success.html.twig');
    }
});

// NOTE: $_SESSION or $_FILES work the same way as they did before

// Run app
$app->run();
