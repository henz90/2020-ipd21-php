<?php 
require_once 'db.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add article</title>
    <link rel="stylesheet" href="styles.css" />
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector:'textarea'});</script>
</head>
<body>
    <div id="centerArticle">
        <?php

function printForm($title = "", $body = "") {
    $title = htmlentities($title); // avoid invalid html in case <>" are part of name
    // $email = htmlentities($body); // just be safe but perhaps not needed for an email
    $form = <<< END
    <form method="post">
        <h1>Title: <input type="text" name="title" value="$title"></h1>
        <br>
        <textarea name="body" cols="60" rows="10">$body</textarea><br>
        <input type="submit" value="Post article">
    </form>
END;
    echo $form;
}
        if (!isset($_SESSION['user'])) { // not logged in
            // if user is not authenticated then do NOT display the form but
            // only "access denied" message with link back to index. php
            echo '<p>You must login first to post an article. <a href="index.php">Click to continue</a>.</p>';
        } else {
            if (isset($_POST['title'])) { // are we receiving a submission?
                $title = $_POST['title'];
                $body = $_POST['body'];
                // FIXME: sanitize body - 1) only allow certain HTML tags, 2) make sure it is valid html
                // WARNING: If you forget to sanitize the body bad things may happen such as JavaScript injection
                $body = strip_tags($body, "<p><ul><li><em><strong><i><b><ol><h3><h4><h5><span>");
                //
                $errorList = array();
                if (strlen($title) < 2 || strlen($title) > 100) {
                    array_push($errorList, "Title must be 2-100 characters long");
                    // keep the title even if invalid
                }
                if (strlen($body) < 2 || strlen($body) > 10000) {
                    array_push($errorList, "Body must be 2-10000 characters long");
                    // keep the body even if invalid
                }
                //
                if ($errorList) { // STATE 2: errors in submission - failed
                    echo "<p>There were problems with your submission:</p>\n<ul>\n";
                    foreach ($errorList as $error) {
                        echo "<li class=\"errorMessage\">$error</li>\n";
                    }
                    echo "</ul>\n";
                    printForm($title, $body);
                } else { // STATE 3: successful submission
                    // On successful submission you need to insert a new record with authorId
                    $userId = $_SESSION['user']['id'];
                    $result = mysqli_query($link, sprintf("INSERT INTO articles VALUES (NULL, '%s', NULL, '%s', '%s')",
                        mysqli_real_escape_string($link, $userId),
                        mysqli_real_escape_string($link, $title),
                        mysqli_real_escape_string($link, $body)));
                    if (!$result) {
                        echo "SQL Query failed: " . mysqli_error($link);
                        exit;
                    }
                    $articleId = mysqli_insert_id($link);
                    echo "<p>Article successfully added</p>";
                    echo '<p><a href="article.php?id='. $articleId .'">Click here to view the new article</a></p>';
                }
            } else { // STATE 1: first display
                printForm();
            }
        }
        ?>
    </div>    
</body>
</html>