<?php 
require_once 'db.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Article</title>
    <link rel="stylesheet" href="styles.css" />
</head>
<body>
    <div id="centerArticle">
    <?php
        if (!isset($_GET['id'])) {
            echo "Error: article id missing in the URL";
            exit;
        }
        $id = mysqli_real_escape_string($link, $_GET['id']);
        $sql = "SELECT a.id, a.authorId, a.creationTS, a.title, a.body, u.name FROM articles as a, users as u "
                . "WHERE a.authorId = u.id AND a.id = '$id' ORDER BY a.id DESC";
        $result = mysqli_query($link, $sql);
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        $article = mysqli_fetch_assoc($result);
        if ($article) {
                echo '<div class="articleBox">';
                $titleEscaped = htmlentities($article['title']);
                echo "<h2>". $titleEscaped ."</h2>\n";
                $datetime = strtotime($article['creationTS']);
                $postedDate = date('M d, Y \a\t H:i:s', $datetime );
                echo "<i>Posted by ". htmlentities($article['name']) . " on " . $postedDate . "</i>\n";
                $body = $article['body']; // not removing any tags this time
                echo "<div class=\"articleBody\">$body</div>\n";
                echo '</div>';
                echo "<script> document.title='" .$titleEscaped . "'; </script>\n";
        } else {
            echo '<h2>Article not found</h2>';
        }
        ?>
    </div>
</body>
</html>