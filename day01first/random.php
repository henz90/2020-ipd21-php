<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        function isInt($value) {
            return (is_numeric($value) && ((int)$value == $value));
        } 
        if (!isset($_GET['min']) || (!isset($_GET['max']))){
            echo '<p class="errorMessage">Error: You must provide min and max in the URL.</p>';
            echo '<br>';
        } else {
            $min = $_GET['min'];
            $max = $_GET['max'];
            if (!isInt($min) || (!isInt($max))){
                echo '<p class="errorMessage">Error: Min and max must be interger numbers.</p>';
                echo '<br>';
            } else {
                if ($min > $max){
                    echo '<p class="errorMessage">Error: Min must be lower than max.</p>';
                    echo '<br>';
                } else {
                    $limit = 10;
                    echo '<p>Random Numbers: ';
                    for ($i = 0; $i < $limit; $i++){
                        echo rand($min,$max);
                        if ($i != $limit - 1){
                            echo ", ";
                        }
                    }
                    echo '</p>';
                }
            }
        }
    ?>
</body>
</html>