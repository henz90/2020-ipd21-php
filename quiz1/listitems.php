<?php 
require_once 'db.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Active Auction</title>
    <link rel="stylesheet" href="styles.css" />
</head>
<body>
    <h1 class="centered">Active Auctions</h1>
    <div id="centerArticle">
        <table class="centered">
            <tr>
                <th>Seller's Name</th>
                <th>Item Description</th>
                <th>Last Bid Price</th>
                <th>Photo</th>
                <th>BID</th>
            </tr>
        <?php
            $sql = "Select * from auctions";
            $result = mysqli_query($link, $sql);
            if (!$result){
                echo "SQL Query Failed: " . mysqli_error($link);
                exit;
            }
            while ($row = mysqli_fetch_assoc($result)){
                $auctionId = $row['id'];
                printf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td><a href="placebid.php?id='. $auctionId .'">BID</a></td></tr>'."\n",
                    $row['sellersName'],    
                    $row['itemDescription'],
                    "$".$row['lastBidPrice'],
                    $row['itemImagePath'] ? "<img width=150 src=" . $row['itemImagePath'] . ">" : ""
                );
            }
        ?>
        </table>
    </div>
</body>
</html>