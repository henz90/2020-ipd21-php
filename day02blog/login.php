<?php 
require_once 'db.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="styles.css" />
</head>
<body>
<h1 class="centered">Login</h1>
<div id="center">
    <?php
        function printForm() {
            $form = <<< END
            <form method="post">
            <table>
                <tr>
                    <td>
                        Email: 
                    </td>
                    <td>
                        <input type="text" name="email">
                    </td>
                </tr>
                <tr>
                    <td>
                        Password: 
                    </td>
                    <td>
                    <input type="password" name="password">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Login">
                    </td>
                </tr>
            </table>
                
            </form>
END;
            echo $form;
        }

        if (isset($_POST['email'])) { // are we receiving a submission?
            $email = $_POST['email'];
            $password = $_POST['password'];
            //
            $result = mysqli_query($link, sprintf("SELECT * FROM users WHERE email='%s'",
                mysqli_real_escape_string($link, $email)));
            if (!$result) {
                echo "SQL Query failed: " . mysqli_error($link);
                exit;
            }
            $userRecord = mysqli_fetch_assoc($result);
            $loginSuccess = false;
            if ($userRecord) {
                if ($userRecord['password'] == $password) {
                    $loginSuccess = true;
                }        
            }
            //
            if (!$loginSuccess) { // STATE 2: errors in submission - failed
                echo '<p class="errorMessage">Email or password invalid. Try again or <a href="register.php">register</a>.</p>';
                printForm();
            } else { // STATE 3: successful submission
                unset($userRecord['password']); // for safety reasons remove the password
                $_SESSION['user'] = $userRecord;
                echo "<p>Login successful</p>";
                echo '<p><a href="index.php">Click here to continue</a></p>';
            }
        } else { // STATE 1: first display
            printForm();
        }
    ?>
    </div>
</body>
</html>