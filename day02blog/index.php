<?php 
require_once 'db.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog main page</title>
    <link rel="stylesheet" href="styles.css" />
</head>
<body>
    <div>
        <div id="center">
        <?php
            if (isset($_SESSION['user'])) { // logged in
                echo "<p>You are logged in as " . $_SESSION['user']['name'] . ". ";
                echo "You can <a href=\"logout.php\">logout</a> or <a href=\"articleadd.php\">post an article</a></p>\n";
            } else { // not logged in
                echo "<p><a href=\"login.php\">login</a> or <a href=\"register.php\">register</a> to post articles and comments.</p>";
            }
        ?>
        </div>
        <div id="centerArticle">
        <h1>Welcome to my blog</h1>
            <?php
                $sql = "SELECT a.id, a.authorId, a.creationTS, a.title, a.body, u.name FROM articles as a, users as u "
                        . "WHERE a.authorId = u.id ORDER BY a.id DESC";
                $result = mysqli_query($link, $sql);
                if (!$result) {
                    echo "SQL Query failed: " . mysqli_error($link);
                    exit;
                }
                while ($article = mysqli_fetch_assoc($result)) {
                    echo '<div class="articlePreviewBox">';
                    echo "<h2><a href=\"article.php?id=". $article['id'] . "\">". htmlentities($article['title']) ."</a></h2>\n";
                    $datetime = strtotime($article['creationTS']);
                    $postedDate = date('M d, Y \a\t H:i:s', $datetime );
                    echo "<i>Posted by ". htmlentities($article['name']) . " on " . $postedDate . "</i>\n";
                    $fullBodyNoTags = strip_tags($article['body']);
                    $bodyPreview = substr(strip_tags($fullBodyNoTags), 0, 100); // FIXME
                    $bodyPreview .= (strlen($fullBodyNoTags) > strlen($bodyPreview)) ? "..." : "";
                    echo "<p>$bodyPreview</p>\n";
                    echo '</div>';
                }
            ?>
        </div>
    </div>
</body>
</html>