<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        if (!isset($_GET['name']) || (!isset($_GET['age']))){
            echo "Error: You must provide name and age in the URL";
        } else {
            $name = $_GET['name'];
            $age = $_GET['age'];
            echo "Hello $name! You are $age years old, nice to meet you!";
        } 
    ?>
</body>
</html>