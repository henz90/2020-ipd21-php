-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Jul 08, 2020 at 07:00 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz1`
--

-- --------------------------------------------------------

--
-- Table structure for table `auctions`
--

CREATE TABLE `auctions` (
  `id` int(11) NOT NULL,
  `itemDescription` varchar(1000) NOT NULL,
  `itemImagePath` varchar(200) DEFAULT NULL,
  `sellersName` varchar(100) NOT NULL,
  `sellersEmail` varchar(320) NOT NULL,
  `lastBidPrice` decimal(10,2) NOT NULL,
  `lastBidderName` varchar(50) DEFAULT NULL,
  `lastBidderEmail` varchar(320) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auctions`
--

INSERT INTO `auctions` (`id`, `itemDescription`, `itemImagePath`, `sellersName`, `sellersEmail`, `lastBidPrice`, `lastBidderName`, `lastBidderEmail`) VALUES
(8, '<p>Cras sodales enim sed rutrum convallis. Pellentesque nec sem sit amet elit aliquet gravida. Vestibulum elit risus, gravida eu lectus a, aliquam convallis ipsum. Vestibulum vitae nisi in arcu dictum facilisis eget non lectus. Cras porttitor pulvinar magna, id sagittis risus pellentesque in. Maecenas sagittis tincidunt justo, ut consectetur velit laoreet in. Etiam fermentum dignissim tincidunt. Aenean porttitor lorem mi, quis finibus leo tempor et.</p>', NULL, 'Henry', 'henz90@hotmail.com', '2.00', 'Aaron', 'a@b.com'),
(10, '<p>asdfasfsdfsdfsdfsd</p>', NULL, 'Henry', 'henz90@hotmail.com', '100.00', NULL, NULL),
(11, '<p>asfdsafsdafdsafd</p>', NULL, 'Jerry', 'jerry@jerry.com', '11.00', NULL, NULL),
(12, '<p>dfgdsfgsdgdsfgd</p>', 'uploads/Question.png.png', 'Henry', 'henz90@hotmail.com', '1.00', NULL, NULL),
(13, '<p>fsdgsds ebe thn</p>', 'uploads/Question1.png', 'Hank', 'h@b.com', '11000.00', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auctions`
--
ALTER TABLE `auctions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `itemImagePath` (`itemImagePath`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auctions`
--
ALTER TABLE `auctions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
